<?php

namespace App\Customer\Message\CommandHandler;

use App\Customer\Message\Command\ActivateCustomerCommand;
use App\Customer\Message\Event\CustomerActivatedEvent;
use App\Customer\Message\Response\CustomerResponse;
use App\Customer\Repository\CustomerRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ActivateCustomerCommandHandler implements MessageHandlerInterface
{
    private CustomerRepositoryInterface $repository;
    private MessageBusInterface $eventBus;

    public function __construct(
        CustomerRepositoryInterface $repository,
        MessageBusInterface $eventBus
    ) {
        $this->repository = $repository;
        $this->eventBus = $eventBus;
    }

    public function __invoke(ActivateCustomerCommand $command): CustomerResponse
    {
        $customer = $this->repository->findById($command->getId());

        if (!$customer) {
            throw new \RuntimeException('Customer with id '.$command->getId().' not found!');
        }

        $customer->activate();

        $event = new CustomerActivatedEvent($customer->getId());
        $this->eventBus->dispatch($event)->with(new DispatchAfterCurrentBusStamp());

        return new CustomerResponse($customer);
    }
}

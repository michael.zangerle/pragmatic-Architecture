<?php

namespace App\Customer\Message\QueryHandler;

use App\Customer\Message\Query\GetCustomerQuery;
use App\Customer\Message\Response\CustomerResponse;
use App\Customer\Repository\CustomerRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetCustomerQueryHandler implements MessageHandlerInterface
{
    private CustomerRepositoryInterface $repository;

    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(GetCustomerQuery $query): CustomerResponse
    {
        $customer = $this->repository->findById($query->getId());

        if (!$customer) {
            throw new \RuntimeException('Customer with id '.$query->getId().' not found!');
        }

        return new CustomerResponse($customer);
    }
}

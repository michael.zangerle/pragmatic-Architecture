<?php

namespace App\Entity;

class Customer
{
    public const TYPES = [
        self::NEW,
        self::REGULAR,
        self::MERCHANT,
    ];

    public const MERCHANT = 'MERCHANT';
    public const REGULAR = 'REGULAR';
    public const NEW = 'NEW';

    private int $id;
    private string $firstName;
    private string $lastName;
    private bool $isActive;
    private string $type;

    public function __construct(int $id, string $firstName, string $lastName, bool $isActive, string $type)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->isActive = $isActive;
        $this->type = $type;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function activate(): void
    {
        $this->isActive = true;
    }

    public function deactivate(): void
    {
        $this->isActive = false;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}

<?php

namespace App\Customer\Repository;

use App\Entity\Customer;

interface CustomerRepositoryInterface
{
    public function findById(int $id): ?Customer;

    /**
     * @return array<Customer>
     */
    public function getAll(?string $firstName, ?string $lastName): array;

    public function getTotal(): int;
}

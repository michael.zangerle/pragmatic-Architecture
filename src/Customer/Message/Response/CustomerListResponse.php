<?php

namespace App\Customer\Message\Response;

use App\Entity\Customer;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class CustomerListResponse
{
    /**
     * @var array<CustomerResponse>
     */
    private array $customers;
    private int $total;

    /**
     * @param array<Customer> $customers
     */
    public function __construct(array $customers, int $total)
    {
        $this->customers = [];
        $this->total = $total;

        foreach ($customers as $customer) {
            $this->customers[] = new CustomerResponse($customer);
        }
    }

    /**
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=CustomerResponse::class)))
     *
     * @return array<CustomerResponse>
     */
    public function getCustomers(): array
    {
        return $this->customers;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}

<?php

namespace App\Customer\Message\Response;

use App\Entity\Customer;

final class CustomerResponse
{
    private Customer $entity;

    public function __construct(Customer $entity)
    {
        $this->entity = $entity;
    }

    public function getId(): int
    {
        return $this->entity->getId();
    }

    public function getFirstName(): string
    {
        return $this->entity->getFirstName();
    }

    public function getLastName(): string
    {
        return $this->entity->getLastName();
    }

    public function isActive(): bool
    {
        return $this->entity->isActive();
    }

    public function getType(): string
    {
        return $this->entity->getType();
    }
}

# Pragmatic Architecture

This example project is part of a blog series. 

## Setup
To get startet on your local machine checkout the repository and install all dependencies with composer.

```
composer install
```

Afterwards setup the messenger queues with the following command:
```
bin/console messenger:setup-transports
```

That's it. 

## APIs

The project offers some example apis. After you started the server (e.g. `symfony server:start`) you can find the 
generated documentation under `https://127.0.0.1:8001/api/docs`.



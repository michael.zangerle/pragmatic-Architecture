<?php

namespace App\Customer\Message\Command;

use Fusonic\HttpKernelExtensions\Dto\RequestDto;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

final class ChangeCustomerTypeCommand implements RequestDto
{
    /**
     * @OA\Property(readOnly=true)
     * @Assert\NotNull(message="Id should not be null.")
     * @Assert\Positive(message="Id should be a positive integer.")
     */
    private int $id;

    /**
     * @Assert\NotNull(message="Type should not be null.")
     * @Assert\Choice(
     *     choices=App\Entity\Customer::TYPES,
     *     message="Customer type should match one of existing types."
     * )
     */
    private string $type;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}

<?php

namespace App\Customer\Message\Event;

use App\Message\AsyncMessageInterface;

final class CustomerActivatedEvent implements AsyncMessageInterface
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}

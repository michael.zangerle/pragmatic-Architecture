<?php

namespace App\Customer\Message\QueryHandler;

use App\Customer\Message\Query\GetCustomersQuery;
use App\Customer\Message\Response\CustomerListResponse;
use App\Customer\Repository\CustomerRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetCustomersQueryHandler implements MessageHandlerInterface
{
    private CustomerRepositoryInterface $repository;

    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(GetCustomersQuery $query): CustomerListResponse
    {
        $customers = $this->repository->getAll($query->getFilter()->getFirstName(), $query->getFilter()->getLastName());
        $total = $this->repository->getTotal();

        return new CustomerListResponse($customers, $total);
    }
}

<?php

namespace App\Customer\Message\Query;

use Fusonic\HttpKernelExtensions\Dto\RequestDto;

final class GetCustomersQuery implements RequestDto
{
    private CustomerFilter $filters;

    public function __construct(?CustomerFilter $filters = null)
    {
        $this->filters = $filters ?? new CustomerFilter();
    }

    public function getFilter(): CustomerFilter
    {
        return $this->filters;
    }

    public function setFilter(CustomerFilter $filters): void
    {
        $this->filters = $filters;
    }
}

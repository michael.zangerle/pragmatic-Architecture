<?php

namespace App\Customer\Controller;

use App\Controller\ControllerResponseTrait;
use App\Customer\Message\Command\ActivateCustomerCommand;
use App\Customer\Message\Command\ChangeCustomerTypeCommand;
use App\Customer\Message\Command\DeactivateCustomerCommand;
use App\Customer\Message\Query\GetCustomerQuery;
use App\Customer\Message\Query\GetCustomersQuery;
use App\Customer\Message\Response\CustomerListResponse;
use App\Customer\Message\Response\CustomerResponse;
use App\Customer\Security\CustomerVoter;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/customers")
 */
final class CustomerController extends AbstractController
{
    use ControllerResponseTrait;

    private MessageBusInterface $queryBus;
    private MessageBusInterface $commandBus;

    public function __construct(
        MessageBusInterface $queryBus,
        MessageBusInterface $commandBus,
        SerializerInterface $serializer
    ) {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
    }

    /**
     * @OA\Tag(name="Customer")
     * @OA\Parameter(name="query", in="query", explode=true, @OA\Schema(ref=@Model(type=GetCustomersQuery::class)))
     * @OA\Response(
     *     @Model(type=CustomerListResponse::class),
     *     response=Response::HTTP_OK,
     *     description="Successful fetch of customers."
     * )
     * @Route("", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function cgetAction(GetCustomersQuery $query): Response
    {
        $this->isGranted(CustomerVoter::LIST);
        $envelope = $this->queryBus->dispatch($query);

        return $this->createJsonResponseFromEnvelope($envelope);
    }

    /**
     * @OA\Tag(name="Customer")
     * @OA\Response(
     *     @Model(type=CustomerResponse::class),
     *     response=Response::HTTP_OK,
     *     description="Successful fetch of customer."
     * )
     * @Route("/{id}", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getAction(GetCustomerQuery $query): Response
    {
        $this->isGranted(CustomerVoter::VIEW);
        $envelope = $this->queryBus->dispatch($query);

        return $this->createJsonResponseFromEnvelope($envelope);
    }

    /**
     * @OA\Tag(name="Customer")
     * @OA\Response(
     *     @Model(type=CustomerResponse::class),
     *     response=Response::HTTP_OK,
     *     description="Successful activation of customer."
     * )
     * @Route("/{id}/activate", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function activateAction(ActivateCustomerCommand $command): Response
    {
        $this->isGranted(CustomerVoter::ACTIVATE);
        $envelope = $this->commandBus->dispatch($command);

        return $this->createJsonResponseFromEnvelope($envelope);
    }

    /**
     * @OA\Tag(name="Customer")
     * @OA\Response(
     *     @Model(type=CustomerResponse::class),
     *     response=Response::HTTP_OK,
     *     description="Successful deactivation of customer."
     * )
     * @Route("/{id}/deactivate", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function deactivateAction(DeactivateCustomerCommand $command): Response
    {
        $this->isGranted(CustomerVoter::DEACTIVATE);
        $envelope = $this->commandBus->dispatch($command);

        return $this->createJsonResponseFromEnvelope($envelope);
    }

    /**
     * @OA\Tag(name="Customer")
     * @OA\RequestBody(@Model(type=ChangeCustomerTypeCommand::class), required=true)
     * @OA\Response(
     *     @Model(type=CustomerResponse::class),
     *     response=Response::HTTP_OK,
     *     description="Successful change of customer type."
     * )
     * @Route("/{id}/changeType", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function changeTypeAction(ChangeCustomerTypeCommand $command): Response
    {
        $this->isGranted(CustomerVoter::CHANGE_ROLE);
        $envelope = $this->commandBus->dispatch($command);

        return $this->createJsonResponseFromEnvelope($envelope);
    }
}

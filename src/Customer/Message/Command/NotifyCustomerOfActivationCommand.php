<?php

namespace App\Customer\Message\Command;

final class NotifyCustomerOfActivationCommand
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
